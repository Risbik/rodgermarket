//
//  DataViewModel.swift
//  rodExpress
//
//  Created by Dell Smith on 23/03/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import Foundation
import UIKit

class GoodsViewModel {
    var container = Dictionary<String, [Item]>()
    
    
    init() {
        
    }
    
    func addToContainer(type: String, item: Item){
        if container.index(forKey: type) != nil {
            container[type]?.append(item)
        } else {
            container.updateValue([item], forKey: type)
        }
    }
    
    func getNumberOfSections() -> Int {
        return container.keys.count
    }
    
    func getNumberOfRows(section: Int) -> Int {
        let key = Array(container.keys)[section]
        
        return container[key]!.count
    }
    
    func getCell(tableView: UITableView, index: Int) -> GoodsViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "goodsCell") as! GoodsViewCell
        
        let key = Array(container.keys)[index]
        cell.collectionView.items = container[key]!
        cell.contentView.layoutIfNeeded()
        return cell
    }
    
    func getItemsNamesFrom(letters: String) -> [String] {
        var hints: [String] = []
        
        for key in Array(container.keys) {
            for item in container[key]! {
                if item.name.starts(with: letters) {
                    if !hints.contains(item.name) {
                        hints.append(item.name)
                    }
                }
            }
        }
        
        return hints
    }
    
    func getItemsCategorysFrom(letters: String) -> [String] {
        var hints: [String] = []
        
        for key in Array(container.keys) {
            for item in container[key]! {
                let nameLetters = item.category.prefix(letters.count)
                if (letters == nameLetters) {
                    if (hints.contains(item.category)) { continue }
                    hints.append(item.category)
                    
                }
            }
        }
        
        return hints
    }
    
    func getItemsFrom(category: String) -> [Item] {
        var result: [Item] = []
        for key in Array(container.keys) {
            for item in container[key]! {
                let nameLetters = item.category
                if (category == nameLetters) { result.append(item) }
            }
        }
        if (result.count != 0) { return result }
        for key in Array(container.keys) {
            for item in container[key]! {
                let nameLetters = item.name
                if (category == nameLetters) { result.append(item) }
            }
        }
        
        return result
    }
}
