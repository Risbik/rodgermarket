//
//  BasketViewModel.swift
//  rodExpress
//
//  Created by Dell Smith on 26/03/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import Foundation
import UIKit

struct BasketItem {
    var item: Item
    var number: Int
}

class BasketViewModel {
    
    fileprivate static var items: [BasketItem] = []
    
    fileprivate init() { }
    
    static func addToBasket(item: Item) {
        for index in 0 ..< items.count {
            if (items[index].item.name == item.name) {
                items[index].number += 1
                return
            }
        }
        items.append(BasketItem(item: item, number: 1))
    }
    
    static func removeFromBasket(item: Item) {
        var newItems: [BasketItem] = []
        
        for index in 0 ..< items.count {
            if (items[index].item.name != item.name) { newItems.append(items[index]) }
        }
        items = newItems
    }
    
    static func removeFromBasket(item: Item, number: Int) {
        for index in 0 ..< items.count {
            if (items[index].item.name == item.name) {
                if (items[index].number <= number) {removeFromBasket(item: items[index].item); return }
                items[index].number -= number
                return
            }
        }
    }
    
    static func getItemsInBasket() -> [BasketItem] {
        return items
    }
    
    static func getTotalCost() -> Int {
        var result = 0
        for item in items {
            result += item.number*item.item.cost
        }
        return result
    }
    
    static func clearItems() {
        items = []
    }
}
