//
//  AfterBuyController.swift
//  rodExpress
//
//  Created by Dell Smith on 03/04/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import UIKit

class AfterBuyController: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.layer.borderColor = UIColor.orange.cgColor
        backButton.layer.borderWidth = 1
        backButton.layer.cornerRadius = 7
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        
    }
    

}
