//
//  ViewController.swift
//  rodExpress
//
//  Created by Dell Smith on 26/03/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import UIKit

class CategoryTabBarController: UITabBarController {
    
    var goodsViewModel: GoodsViewModel!
    var items: [Item] = []
    var ads: [Ads] = []
    let basketButton = BasketButton(frame: CGRect(x: 0, y: 0, width: 80, height: 30))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        basketButton.addTarget(self, action: #selector(basketButtonPressed), for: .touchUpInside)
        navigationController?.navigationBar.isHidden = false
        self.navigationController!.visibleViewController!.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: basketButton)
        
        goodsViewModel = GoodsViewModel()
        calculate()
        let firstItem = viewControllers![0] as! CategoryViewController
        firstItem.goodsViewModel = goodsViewModel
        firstItem.ads = ads
        firstItem.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "123", style: .plain, target: nil, action: nil)
        
        let secondItem = viewControllers![1] as! SearchViewController
        secondItem.goodsViewModel = goodsViewModel
    }
    
    func calculate() {
        for item in items {
            goodsViewModel.addToContainer(type: item.category, item: item)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        basketButton.updateBadge()
    }
    
    @objc func basketButtonPressed() {
        let newView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "basketView") as! BasketViewController
        navigationController?.pushViewController(newView, animated: true)
    }
}
