//
//  SearchViewController.swift
//  rodExpress
//
//  Created by Dell Smith on 26/03/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, GoodsCellDelegate {
    
    
    @IBOutlet weak var searchHints: UITableView!
    @IBOutlet weak var searchResult: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var searchString = ""
    var hints: [[String]] = [[""], [""]]
    var goodsViewModel: GoodsViewModel!
    var itemsToDisplay: [Item] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchHints.tag = 0
        searchResult.tag = 1
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let toolBar = KeyboardToolbar.getKeyboardToolbar(view: view)
        toolBar.sizeToFit()
        searchBar.inputAccessoryView = toolBar
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView.tag {
        case 0:
            return hints.count
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 0:
            return hints[section].count
        default:
            return (itemsToDisplay.count == 0) ? 0 : 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "hintCell") as! HintCell
            let ind1 = indexPath.section, ind2 = indexPath.row
            cell.label.text = hints[ind1][ind2]
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "goodsCell") as! GoodsViewCell
            cell.collectionView.items = itemsToDisplay
            cell.collectionView._delegate = self
            cell.collectionView.reloadData()
            cell.contentView.layoutIfNeeded()
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableView.tag == 1) { return }
        let ind1 = indexPath.section
        let ind2 = indexPath.row
        searchString = hints[ind1][ind2]
        itemsToDisplay = goodsViewModel.getItemsFrom(category: searchString)
        searchHints.isHidden = true
        searchBar.text = searchString
        self.view.endEditing(true)
        searchResult.reloadData()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if (tableView.tag == 0) {
            let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
            footerView.backgroundColor = UIColor.clear
            return footerView
        }
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        footerView.backgroundColor = UIColor(red: 255 / 255, green: 120 / 255, blue: 1 / 255, alpha: 1)
        return footerView
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if (section == 0) {       
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
            headerView.backgroundColor = UIColor.orange
            return headerView
        }
        
        return nil
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        emptyHints()
        self.view.endEditing(true)
        self.searchBar.text = ""
        itemsToDisplay = []
        searchResult.reloadData()
        searchHints.isHidden = true
    }
    
    func emptyHints() {
        searchString = ""
        hints = [[""], [""]]
        searchHints.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == "" {
            searchHints.isHidden = true
            emptyHints()
            return
        }
        
        hints[0] = goodsViewModel.getItemsNamesFrom(letters: searchText)
        hints[1] = goodsViewModel.getItemsCategorysFrom(letters: searchText)
        searchHints.isHidden = false
        searchHints.reloadData()
    }
    
    func goodsCellPressed(item: Item) {
        let newView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailedView") as! DetailedViewController
        newView.item = item
        navigationController?.pushViewController(newView, animated: true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if (searchBar.text == "") {
            return
        } else {
            searchString = searchBar.text!
            
            let itemNames = goodsViewModel.getItemsNamesFrom(letters: searchString)
            
            for name in itemNames {
                itemsToDisplay += goodsViewModel.getItemsFrom(category: name)
            }
            
            searchHints.isHidden = true
            searchBar.text = searchString
            searchResult.reloadData()
            
            self.view.endEditing(true)
        }
        
    }
}
