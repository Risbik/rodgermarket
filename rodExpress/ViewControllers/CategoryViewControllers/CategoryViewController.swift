//
//  PartnersViewController.swift
//  rodExpress
//
//  Created by Dell Smith on 25/03/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, GoodsCellDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var items: [Item] = []
    var ads: [Ads] = []
    var specialItem: [SpecialItem] = []
    var goodsViewModel: GoodsViewModel!
    var categoryButtonEnabled = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        navigationController!.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "123", style: .done, target: nil, action: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3 + Array(goodsViewModel.container.keys).count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        let key = Array(goodsViewModel.container.keys)[section - 3]
        //        let items = goodsViewModel.container[key]
        //        return items!.count
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "adsCell", for: indexPath) as! AdsTableViewCell
            cell.ads = ads
            cell.load()

            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! MenuViewCell
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "specialCell", for: indexPath) as! SpecialTableViewCell
            cell.items = specialItem
            cell.load()
            cell.contentView.layoutIfNeeded()
            return cell
        default:
            let cell = goodsViewModel.getCell(tableView: tableView, index: indexPath.section - 3)
//            cell.collectionView.borderColor = UIColor.orange
            cell.collectionView._delegate = self
            cell.collectionView.reloadData()
            cell.contentView.layoutIfNeeded()
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            if (ads.count == 0) { return 30 }
            return 120
        case 1:
            return 120
        case 2:
            return 125
        default:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if (section == 1) {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
            headerView.backgroundColor = UIColor.white
            return headerView
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if (section == 0) {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
            headerView.backgroundColor = UIColor.white
            return headerView
        }
        else if (section == 1) {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
            headerView.backgroundColor = UIColor(red: 255 / 255, green: 120 / 255, blue: 1 / 255, alpha: 1)
            return headerView
        }
        else if (section == 2) {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
            headerView.backgroundColor = UIColor.white
            return headerView
        }
        let header = Array(goodsViewModel.container.keys)[section - 3]
        let headerView = getHeaderView(header: header, tableView: tableView, tag: section - 3)
        return headerView
    }
    
    func getHeaderView(header: String, tableView: UITableView, tag: Int) -> UIView {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        headerView.backgroundColor = UIColor(red: 247 / 255, green: 159 / 255, blue: 103 / 255, alpha: 1)
        let label = UILabel(frame: CGRect(x: 45, y: -5, width: headerView.bounds.size.width - 30, height: 20))
        label.text = header
        label.textAlignment = .left
        label.font = UIFont(name: "Futura", size: 17)
        label.textColor = UIColor.white
        
        if (categoryButtonEnabled) {
            let button = UIButton(frame: CGRect(x: 45, y: -5, width: headerView.bounds.size.width - 30, height: 20))
            button.tag = tag
            button.addTarget(self, action: #selector(categoryPressed), for: .touchUpInside)
            button.titleLabel?.textAlignment = .left
            button.titleLabel?.font = UIFont(name: "Font", size: 17)
            button.titleLabel?.textColor = UIColor.white
            headerView.addSubview(button)
            
            let image = UIImageView(frame: CGRect(x: headerView.bounds.size.width - 30 - 45, y: -5, width: 20, height: 20))
            image.image = UIImage(named: "arrow")
            image.backgroundColor = UIColor.clear
            
            headerView.addSubview(image)
        }

        headerView.addSubview(label)
        
        
        
        return headerView
    }
    
    @objc func categoryPressed(_ sender: UIButton) {
        print("clicked")
        
        let tabBar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailedTabBar") as! CategoryTabBarController
        let newView = tabBar.viewControllers![0] as! CategoryViewController
        let key = Array(goodsViewModel.container.keys)[sender.tag]
        newView.title = key
        tabBar.goodsViewModel = GoodsViewModel()
        
        tabBar.items = goodsViewModel.container[key]!
        var newContainer =  Dictionary<String, [Item]>()
        newContainer.updateValue(newView.items, forKey: key)
        tabBar.goodsViewModel.container = newContainer
        newView.categoryButtonEnabled = false
        navigationController?.pushViewController(tabBar, animated: true)
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (section < 2) {
            return 0.00001
        }
        else { return 30 }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if (section < 2) {
            return 0.000001
        }
        else { return 30 }
    }
    
    func goodsCellPressed(item: Item) {
        let newView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailedView") as! DetailedViewController
        newView.item = item
        navigationController?.pushViewController(newView, animated: true)
    }
}
