//
//  ViewController.swift
//  rodExpress
//
//  Created by Dell Smith on 23/03/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import UIKit

class MainScreenViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, GoodsCellDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var goodsVIewModel: GoodsViewModel!
    var ads: [Ads] = []
    var menuItems: [MenuItem] = []
    var specialItems: [SpecialItem] = []
    var partners: [Partner] = []
    var searchView: SearchViewController!
    var searchHints: UITableView!
    var searchResult: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        goodsVIewModel = GoodsViewModel()
        loadItems()
        loadAds()
        loadSpecial()
        loadPartners()
        
//        self.navigationController!.visibleViewController!.navigationItem.rightBarButtonItem = nil
        navigationController?.navigationBar.barStyle = .black
        let toolBar = KeyboardToolbar.getKeyboardToolbar(view: view)
        toolBar.sizeToFit()
        searchBar.inputAccessoryView = toolBar
        
        let searchItem = tabBarController!.viewControllers![2] as! SearchViewController
        searchItem.goodsViewModel = goodsVIewModel
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
        tabBarController?.tabBar.isHidden = false
        
        let toolBar = KeyboardToolbar.getKeyboardToolbar(view: view)
        toolBar.sizeToFit()
        searchBar.inputAccessoryView = toolBar
        
        updateBasketBadge()
    }
    
    // Make the Status Bar Light/Dark Content for this View
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
        //return UIStatusBarStyle.default   // Make dark again
    }
    
    func loadItems() {
        let items = JSONParser.parseItems()
        for item in items {
            for adr in item.imgAdr {
                let url = URL(string: adr)
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                item.images.append(UIImage(data: data!)!)
            }
            goodsVIewModel.addToContainer(type: item.type, item: item)
        }
    }
    func loadAds() {
        let items = JSONParser.parseAds()
        for item in items {
            let url = URL(string: item.imgAdr)
            let data = try? Data(contentsOf: url!)
            item.image = UIImage(data: data!)
            item.url = URL(string: item.urlString)!
            ads.append(item)
        }
    }
    
    func loadSpecial() {
        specialItems = JSONParser.parseSpecial()
        
        for item in specialItems {
            let url = URL(string: item.imgAdr)
            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            item.image = UIImage(data: data!)!
        }
    }
    
    func loadPartners() {
        partners = JSONParser.parsePartners()
        
        for item in partners {
            let url = URL(string: item.imgAdr)
            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            item.image = UIImage(data: data!)!
            item.siteUrl = URL(string: item.site)!
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (3 + Array(goodsVIewModel.container.keys).count)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return 1
        case 3:
            return 1
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "adsCell", for: indexPath) as! AdsTableViewCell
            cell.ads = ads
            cell.load()
            return cell
//        case 1:
//            let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! MenuViewCell
//            cell.items = menuItems
//            cell.load()
//            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "specialCell", for: indexPath) as! SpecialTableViewCell
            cell.items = specialItems
            cell.load()
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "partnerCell", for: indexPath) as! PartnerViewCell
            cell.partners = partners
            cell.load()
            return cell
        default:
            let cell = goodsVIewModel.getCell(tableView: tableView, index: indexPath.section - 3)
            cell.contentView.layoutIfNeeded()
            cell.collectionView._delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 150
        case 1:
            return 120
        case 2:
            return 125
        default:
            return 100
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if (section == 1) {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
            headerView.backgroundColor = UIColor(red: 255 / 255, green: 120 / 255, blue: 1 / 255, alpha: 1)
            return headerView
        }
        else if (section == 2) {
            return getCellHeaderView(title: "Производители",
                                     buttonActive: false,
                                     tag: section - 2,
            backgrColor: UIColor.white)
        }
        else if (section > 2) {
            return getCellHeaderView(title: Array(goodsVIewModel.container.keys)[section - 3],
            buttonActive: true,
            tag: section - 2,
            backgrColor: UIColor(red: 247 / 255, green: 159 / 255, blue: 103 / 255, alpha: 1))
        }
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        headerView.backgroundColor = UIColor(red: 255 / 255, green: 120 / 255, blue: 1 / 255, alpha: 1)
        return headerView
    }
//    UIColor(red: 247 / 255, green: 159 / 255, blue: 103 / 255, alpha: 1)
    func getCellHeaderView(title: String?, buttonActive: Bool, tag: Int, backgrColor: UIColor) -> UIView {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        headerView.backgroundColor = backgrColor
        let label = UILabel(frame: CGRect(x: 45, y: -5, width: headerView.bounds.size.width - 30, height: 20))
        label.text = title
        label.textAlignment = .left
        label.font = UIFont(name: "Futura", size: 17)
        label.textColor = UIColor.white
        
        if (buttonActive) {
        let button = UIButton(frame: CGRect(x: 45, y: -5, width: headerView.bounds.size.width - 30, height: 20))
        button.tag = tag
        button.addTarget(self, action: #selector(categoryPressed), for: .touchUpInside)
        button.titleLabel?.textAlignment = .left
        button.titleLabel?.font = UIFont(name: "Font", size: 17)
        button.titleLabel?.textColor = UIColor.white
        
        let image = UIImageView(frame: CGRect(x: headerView.bounds.size.width - 30 - 45, y: -5, width: 20, height: 20))
        image.image = UIImage(named: "arrow")
        image.backgroundColor = UIColor.clear
            headerView.addSubview(image)
             headerView.addSubview(button)
        }
        
        headerView.addSubview(label)
       
        
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if (section == 0) {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
            headerView.backgroundColor = UIColor(red: 255 / 255, green: 120 / 255, blue: 1 / 255, alpha: 1)
            return headerView
        }
        else if (section == 1) {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
            headerView.backgroundColor = UIColor.white
            return headerView
        }
        else if (section >= 2) {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
            headerView.backgroundColor = UIColor(red: 247 / 255, green: 159 / 255, blue: 103 / 255, alpha: 1)
            return headerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (section == 0) {return 0.000001}
        else { return 30 }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    @objc func categoryPressed(_ sender: UIButton) {
        print("clicked")
        
        if (sender.tag > 0) {
            
            let newView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailedTabBar") as! CategoryTabBarController
            
            let key = Array(goodsVIewModel.container.keys)[sender.tag - 1]
            newView.title = key
            newView.navigationItem.backBarButtonItem?.tintColor = UIColor.white
            newView.items = goodsVIewModel.container[key]!
            for item in ads {
                if (item.category == key) {
                    var ad = Ads()
                    ad.category = item.category
                    ad.image = item.image
                    newView.ads.append( ad )
                }
            }
            tabBarController?.tabBar.isHidden = true
            navigationController?.pushViewController(newView, animated: true)
            
        }
    }
    
    func goodsCellPressed(item: Item) {
        let newView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailedView") as! DetailedViewController
        newView.item = item
        navigationController?.pushViewController(newView, animated: true)
    }
    
    func updateBasketBadge() {
        let items = BasketViewModel.getItemsInBasket()
        let basketItem = tabBarController!.tabBar.items![1]
        if (items.count == 0) { basketItem.badgeValue = "0"; return }
        
        var value = 0
        
        for item in items {
            value += item.number*item.item.cost
        }
        basketItem.badgeValue = String(value)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let searchItem = tabBarController!.viewControllers![2] as! SearchViewController
        searchItem.hints[0] = goodsVIewModel.getItemsNamesFrom(letters: searchText)
        searchItem.hints[1] = goodsVIewModel.getItemsCategorysFrom(letters: searchText)
        searchBar.text = ""
        tabBarController!.selectedIndex = 2
        searchItem.searchBar.text = searchText
    }
}
