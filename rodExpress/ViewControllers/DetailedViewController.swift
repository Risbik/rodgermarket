//
//  DetailedViewController.swift
//  rodExpress
//
//  Created by Dell Smith on 25/03/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import UIKit

class DetailedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var toBasketButton: UIButton!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var item: Item!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        
        buyButton.layer.cornerRadius = 10
        toBasketButton.layer.cornerRadius = 10
        
                let rightBarButton = BasketButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        rightBarButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        rightBarButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        navigationController!.visibleViewController?.navigationController!.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightBarButton)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false        
        tabBarController?.tabBar.isHidden = true
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "imageCell") as! ImageContainer
            cell.images = item.images
            cell.load()
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCell") as! TextContainer
            cell.textView.text =  "Название: " + item.name + "\n"
            cell.textView.text += "Цена: " + String(item.cost) + "\n"
            cell.textView.text += "Категория: " + item.type
            cell.textView.layer.cornerRadius = 10
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCell") as! TextContainer
            cell.textView.text = " " + item.desc
            cell.textView.layer.cornerRadius = 10
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0: do {
            return 120
            }
        default:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        footer.backgroundColor = UIColor.orange
        return footer        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        header.backgroundColor = UIColor.orange
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0.00001
        default:
            return 30
        }
    }
    
    @IBAction func addToBasket(_ sender: Any) {
        BasketViewModel.addToBasket(item: item)        
        addToBasketAnim()
    }
    
    func addToBasketAnim() {
        UIView.animate(withDuration: 0.6) {
            self.buyButton.backgroundColor = UIColor.white
            self.buyButton.backgroundColor = UIColor.orange
        }
    }
    
    @IBAction func toBasket(_ sender: Any) {
        let newView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "basketView") as! BasketViewController
        navigationController?.pushViewController(newView, animated: true)        
    }
    
}
