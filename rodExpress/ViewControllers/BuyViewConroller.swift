//
//  BuyViewConroller.swift
//  rodExpress
//
//  Created by Dell Smith on 03/04/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import UIKit

class BuyViewConroller: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var errorView: UIView!
    
    @IBOutlet weak var errorMsg: UILabel!
    var showCardTypes = false
    var payViaCard = true
    var checkCell: CheckCell!
    var preloadData = false
    var dataChanged = false
    var userData = UserData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let doneButton = UIBarButtonItem(title: "Готово", style: .plain, target: self, action: #selector(donePressed))
        self.navigationController!.visibleViewController!.navigationItem.rightBarButtonItem = doneButton
        
        if let userCard = UserDefaults.standard.value(forKey: "CardNumber") {
            preloadData = true
            userData.cardNumber = userCard as! String
        }
        
        if let userCard = UserDefaults.standard.value(forKey: "Country") {
            preloadData = true
            userData.userAddress.country = userCard as! String
        }
        
        if let userCard = UserDefaults.standard.value(forKey: "City") {
            preloadData = true
            userData.userAddress.city = userCard as! String
        }
        
        if let userCard = UserDefaults.standard.value(forKey: "Street") {
            preloadData = true
            userData.userAddress.street = userCard as! String
        }
        
        if let userCard = UserDefaults.standard.value(forKey: "House") {
            preloadData = true
            userData.userAddress.house = userCard as! String
        }
        
        if let userCard = UserDefaults.standard.value(forKey: "Appartsments") {
            preloadData = true
            userData.userAddress.appartmens = userCard as! String
        }
        
        if let userCard = UserDefaults.standard.value(forKey: "Index") {
            preloadData = true
            userData.userAddress.index = userCard as! String
        }
        
        if (preloadData) {
            print(userData.cardNumber)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIWindow.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIWindow.keyboardWillHideNotification, object: nil)
    }
    
    @objc func donePressed() {
        var errorMsg = ""
        if (userData.cardNumber.count < 4) {
            errorMsg = "Неправильно введен номер банковской карты \n"
        }
        if (userData.userAddress.country.count < 4) {
            errorMsg += "Неправильно введено название страны \n"
        }
        if (userData.userAddress.city.count < 4) {
            errorMsg += "Неправильно введено название города \n"
        }
        if (userData.userAddress.street.count < 4) {
            errorMsg += "Неправильно введено название улицы \n"
        }
        if (userData.userAddress.house.count < 1) {
            errorMsg += "Неправильно введен номер дома \n"
        }
        if (userData.userAddress.appartmens.count < 4) {
            errorMsg += "Неправильно введен номер квартиры \n"
        }
        if (userData.userAddress.index.count < 4) {
            errorMsg += "Неправильно введен почтовый индекс"
        }
        
        
        if (errorMsg != "") {
            self.errorMsg.text = errorMsg
            UIView.animate(withDuration: 0.5, animations: {
                self.errorView.alpha = 1
            }) { (true) in
                UIView.animate(withDuration: 0.5, delay: 2, options: .curveEaseInOut, animations: {
                    self.errorView.alpha = 0
                }, completion: nil)
            }
            return
        }
        if (checkCell.check.isOn) {
            if (!preloadData) {
                saveUserData()
            } else {
                if (dataChanged) {
                    showSortAllert()
                }
            }
        }

    }
    
    func proceedBuy() {
        BasketViewModel.clearItems()
        tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        let newView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "afterBuyView") as! AfterBuyController
        navigationController?.pushViewController(newView, animated: true)
        newView.navigationController!.visibleViewController!.navigationItem.hidesBackButton = true
        newView.navigationController!.navigationBar.isHidden = true
    }
    
    func showSortAllert() {
        let alertController = UIAlertController(title: nil, message: "Предшествующие пользовательские данные будут перезаписаны. Продолжить?", preferredStyle: .actionSheet)
        
        let defaultAction = UIAlertAction(title: "Да", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.saveUserData()
        })
        
        let deleteAction = UIAlertAction(title: "Нет", style: .default, handler: { (alert: UIAlertAction!) -> Void in
           
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
            //
        })
        
        alertController.addAction(defaultAction)
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        
//        if let popoverController = alertController.popoverPresentationController {
//            popoverController.barButtonItem = sender
//        }
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func saveUserData() {
        let deffaults = UserDefaults.standard
        deffaults.setValue(userData.cardNumber, forKey: "CardNumber")
        deffaults.setValue(userData.userAddress.country, forKey: "Country")
        deffaults.setValue(userData.userAddress.city, forKey: "City")
        deffaults.setValue(userData.userAddress.street, forKey: "Street")
        deffaults.setValue(userData.userAddress.house, forKey: "House")
        deffaults.setValue(userData.userAddress.appartmens, forKey: "Appartsments")
        deffaults.setValue(userData.userAddress.index, forKey: "Index")
        
        proceedBuy()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            if (showCardTypes) {
                return (payViaCard) ? 4 : 3
            }
            return (payViaCard) ? 2 : 1
        default:
            return 6
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "userInputCell") as! UserInputCell
                cell.label.text = "Стоимость товаров"
                cell.textField.text = String(BasketViewModel.getTotalCost())
                cell.textField.isEnabled = false
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "checkCell") as! CheckCell
                checkCell = cell
                cell.label.text = "Сохранить пользовательские данные"
                return cell
            }
        case 1:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "dropDownCell") as! DropDownCell
                cell.label.text = "Способ оплаты"
                cell.imgView.transform = CGAffineTransform(rotationAngle: .pi * 1 / 2)
                return cell
            case 1:
                if (payViaCard) {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "userInputCell") as! UserInputCell
                    cell.label.text = "Номер карты"
                    cell.textField.placeholder = "Введите номер карты"
                    cell.textField.text = userData.cardNumber
                    cell.textField.keyboardType = .decimalPad
                    cell.textField.inputAccessoryView = KeyboardToolbar.getKeyboardToolbar(view: self.view)
                    cell.textField.delegate = self
                    cell.textField.tag = 0
                    return cell
                }
                let cell = tableView.dequeueReusableCell(withIdentifier: "dropDownItem") as! DropDownItem
                cell.label.text = "Оплата при получении"
                return cell
            case 2:
                if (payViaCard) {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "dropDownItem") as! DropDownItem
                    cell.label.text = "Оплата при получении"
                    return cell
                }
                let cell = tableView.dequeueReusableCell(withIdentifier: "dropDownItem") as! DropDownItem
                cell.label.text = "Оплата картой"
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "dropDownItem") as! DropDownItem
                cell.label.text = "Оплата картой"
                return cell
            }
        default:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "userInputCell") as! UserInputCell
                cell.label.text = "Страна"
                cell.textField.text = userData.userAddress.country
                cell.textField.delegate = self
                cell.textField.tag = 1
                cell.textField.inputAccessoryView = KeyboardToolbar.getKeyboardToolbar(view: self.view)
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "userInputCell") as! UserInputCell
                cell.label.text = "Город"
                cell.textField.text = userData.userAddress.city
                cell.textField.tag = 2
                cell.textField.delegate = self
                cell.textField.inputAccessoryView = KeyboardToolbar.getKeyboardToolbar(view: self.view)
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "userInputCell") as! UserInputCell
                cell.label.text = "Улица"
                cell.textField.text = userData.userAddress.street
                cell.textField.tag = 3
                cell.textField.delegate = self
                cell.textField.inputAccessoryView = KeyboardToolbar.getKeyboardToolbar(view: self.view)
                return cell
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "userInputCell") as! UserInputCell
                cell.label.text = "Дом"
                cell.textField.text = userData.userAddress.house
                cell.textField.tag = 4
                cell.textField.delegate = self
                cell.textField.inputAccessoryView = KeyboardToolbar.getKeyboardToolbar(view: self.view)
                return cell
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: "userInputCell") as! UserInputCell
                cell.label.text = "Квартира"
                cell.textField.text = userData.userAddress.appartmens
                cell.textField.tag = 5
                cell.textField.delegate = self
                cell.textField.inputAccessoryView = KeyboardToolbar.getKeyboardToolbar(view: self.view)
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "userInputCell") as! UserInputCell
                cell.label.text = "Почтовый индекс"
                cell.textField.text = userData.userAddress.index
                cell.textField.tag = 6
                cell.textField.delegate = self
                cell.textField.inputAccessoryView = KeyboardToolbar.getKeyboardToolbar(view: self.view)
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.section == 1) {
            if (payViaCard) {
                if (indexPath.row == 2) {
                    payViaCard = false
                    showCardTypes = false
                    tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
                    return
                }
                else if (indexPath.row == 3) {
                    payViaCard = true
                    showCardTypes = false
                    tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
                    return
                }
            }
            
            if (indexPath.row == 0) {
                payViaCard = false
                showCardTypes = !showCardTypes
                tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
                return
            }
            else if (indexPath.row == 1) {
                if (!payViaCard) {
                    payViaCard = false
                    showCardTypes = false
                    tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
                    return
                }
            }
            else if (indexPath.row == 2) {
                payViaCard = true
                showCardTypes = false
                tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
                return
            }
            
        }
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        })
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            if (userData.cardNumber != textField.text) {
                userData.cardNumber = textField.text
                dataChanged = true
            }
        case 1:
            if (userData.userAddress.country != textField.text) {
                userData.userAddress.country = textField.text
                dataChanged = true
            }
        case 2:
            if (userData.userAddress.city != textField.text) {
                userData.userAddress.city = textField.text
                dataChanged = true
            }
        case 3:
            if (userData.userAddress.street != textField.text) {
                userData.userAddress.street = textField.text
                dataChanged = true
            }
        case 4:
            if (userData.userAddress.house != textField.text) {
                userData.userAddress.house = textField.text
                dataChanged = true
            }
        case 5:
            if (userData.userAddress.appartmens != textField.text) {
                userData.userAddress.appartmens = textField.text
                dataChanged = true
            }
        default:
            if (userData.userAddress.index != textField.text) {
                userData.userAddress.index = textField.text
                dataChanged = true
            }
        }
    }
    
}
