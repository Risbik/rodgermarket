//
//  BasketViewController.swift
//  rodExpress
//
//  Created by Dell Smith on 26/03/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import UIKit

protocol BasketCellDelegate {
    func updateTotalCost()
}

class BasketViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, BasketCellDelegate {
    
    @IBOutlet weak var emptyMessage: UILabel!
    var goodsViewModel: GoodsViewModel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var goToPay: UIButton!    
    @IBOutlet weak var totalCost: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        goToPay.layer.cornerRadius = 10
        goToPay.layer.borderColor = UIColor.orange.cgColor
        goToPay.layer.borderWidth = 1
        
        totalCost.layer.cornerRadius = 10
        totalCost.layer.borderColor = UIColor.orange.cgColor
        totalCost.layer.borderWidth = 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
        navigationController?.navigationBar.backgroundColor = UIColor.orange
        let cost = BasketViewModel.getTotalCost()
        if (cost == 0) {
            totalCost.isHidden = true
            goToPay.isHidden = true
            emptyMessage.isHidden = false
        }
        else {
            totalCost.text = "Стоимость: " + String(cost)
            totalCost.isHidden = false
            goToPay.isHidden = false
            emptyMessage.isHidden = true
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0) { return BasketViewModel.getItemsInBasket().count }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "basketCell") as! BasketViewCell
            
            cell._delegate = self
            cell.basketItem = BasketViewModel.getItemsInBasket()[indexPath.row]
            cell.load()
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "basketViewBackground") as! BasketBackgroundCell
            
            cell.background.image = UIImage(named: "background")
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "basketViewBackground") as! BasketBackgroundCell
            
            cell.background.image = UIImage(named: "background2")
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (section == 0) { return 0.0001 }
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if (section == 2) {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
            headerView.backgroundColor = UIColor.white
            return headerView
        }
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        headerView.backgroundColor = UIColor(red: 255 / 255, green: 120 / 255, blue: 1 / 255, alpha: 1)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if (section == 0) {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
            headerView.backgroundColor = UIColor(red: 255 / 255, green: 120 / 255, blue: 1 / 255, alpha: 1)
            return headerView
        }
        else if (section == 1) {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
            headerView.backgroundColor = UIColor.white
            return headerView
        }
        else {
            return nil
        }
    }
    
    @IBAction func goToPay(_ sender: Any) {
        let newView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "buyView") as! BuyViewConroller
        navigationController?.pushViewController(newView, animated: true)
    }
    
    func updateTotalCost() {
        let cost = BasketViewModel.getTotalCost()
        if (cost == 0) {
            totalCost.text = "Тут пусто"
        }
        else {
            totalCost.text = "Стоимость: " + String(cost)
        }
    }
    
}
