//
//  BasketButton.swift
//  rodExpress
//
//  Created by Dell Smith on 01/04/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import UIKit

class BasketButton: UIButton {
    fileprivate var backgroundImg: UIImage = #imageLiteral(resourceName: "basket")
    fileprivate var badge: UILabel!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        badge = UILabel(frame: CGRect(x: 20, y: 0, width: 15, height: 15))
        badge.backgroundColor = UIColor.red
        badge.textColor = UIColor.white
        badge.layer.cornerRadius = 7
        badge.clipsToBounds = true
        badge.textAlignment = .center
        updateBadge()
        
        addSubview(badge)
        badge.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        badge.bottomAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        badge.sizeToFit()
        
        setImage(backgroundImg, for: .normal)
        tintColor = UIColor.white
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateBadge() {
        let items = BasketViewModel.getItemsInBasket()
        if (items.count == 0) { badge.text = "0"; return }
        
        var value = 0
        
        for item in items {
            value += item.number*item.item.cost
        }
        badge.text = String(value)
        badge.sizeToFit()
    }
    
    func setBadgeString(string: String) {
        badge.text = string
    }
}
