//
//  KeyboardToolbar.swift
//  rodExpress
//
//  Created by Dell Smith on 01/04/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import UIKit

class KeyboardToolbar: UIToolbar {
    static fileprivate var toolBar: KeyboardToolbar?
    static fileprivate var view: UIView!
    fileprivate override init(frame: CGRect) {
        super.init(frame: frame)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    static func getKeyboardToolbar(view: UIView) -> KeyboardToolbar {
        if (toolBar == nil) {
            toolBar = KeyboardToolbar()
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done,
                                             target: self,
                                             action: #selector(KeyboardToolbar.hideKeyboard))
            let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
            toolBar!.setItems([flexSpace, doneButton], animated: false)
        }
        KeyboardToolbar.view = view
        return toolBar!
    }
    
    @objc static func hideKeyboard() {
        KeyboardToolbar.view.endEditing(true)
    }
}
