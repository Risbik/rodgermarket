//
//  JSONParser.swift
//  rodExpress
//
//  Created by Dell Smith on 28/03/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import Foundation
import UIKit

class JSONParser {
    private init () {}
    
    static func parseItems() -> [Item] {
        let path = (Bundle.main.path(forResource: "Items", ofType: "json"))
        if (path == nil) {
            print("File can't be found")
            return []
        }
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
            let decoder = JSONDecoder()
            
            let items = try! decoder.decode(ItemContainer.self, from: data)
            print(items)

            return items.Items
            
        } catch let error {
            print("failed to convert data \(error)")
            return []
        }
    }
    
    static func parseAds() -> [Ads] {
        let path = (Bundle.main.path(forResource: "Ads", ofType: "json"))
        if (path == nil) {
            print("File can't be found")
            return []
        }
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
            let decoder = JSONDecoder()
            

            let items = try! decoder.decode(AdsContainer.self, from: data)
            print(items)
            
            return items.Ads
            
        } catch let error {
            print("failed to convert data \(error)")
            return []
        }
    }

    static func parseSpecial() -> [SpecialItem] {
        let path = (Bundle.main.path(forResource: "SpecialItem", ofType: "json"))
        if (path == nil) {
            print("File can't be found")
            return []
        }
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
            let decoder = JSONDecoder()
            
            
            let items = try! decoder.decode(SpecialItemContainer.self, from: data)
            print(items)
            
            return items.specItems
            
        } catch let error {
            print("failed to convert data \(error)")
            return []
        }
    }
    
    static func parsePartners() -> [Partner] {
        let path = (Bundle.main.path(forResource: "Partners", ofType: "json"))
        if (path == nil) {
            print("File can't be found")
            return []
        }
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
            let decoder = JSONDecoder()
            
            
            let items = try! decoder.decode(PartnerContainer.self, from: data)
            print(items)
            
            return items.partners
            
        } catch let error {
            print("failed to convert data \(error)")
            return []
        }
    }
    
}
