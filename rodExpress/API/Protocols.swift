//
//  Protocols.swift
//  rodExpress
//
//  Created by Dell Smith on 29/03/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import Foundation

protocol GoodsCellDelegate {
    func goodsCellPressed(item: Item)
}

protocol SearchDelegate {
    func searchEnd(on: String)
}

protocol DontButtonDelegate {
    func doneButtonPressed()
}
