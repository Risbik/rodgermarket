//
//  Partner.swift
//  rodExpress
//
//  Created by Dell Smith on 23/03/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import Foundation
import UIKit

class PartnerContainer: Codable {
    var partners: [Partner] = []
    
    init() {
        
    }
}

class Partner: Codable {
    var image: UIImage!
    var imgAdr: String = ""
    var produce: String = ""
    var site: String = ""
    var siteUrl: URL!
    var name: String = ""
    
    init() {
        
    }
    
    enum CodingKeys: String, CodingKey
    {
        case imgAdr = "image"
        case site = "site"
        case name = "name"
        case produce = "produce"
    }
}
