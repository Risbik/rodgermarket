//
//  SpecialItem.swift
//  rodExpress
//
//  Created by Dell Smith on 23/03/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import Foundation
import UIKit

class SpecialItemContainer: Codable {
    var specItems: [SpecialItem] = []
    
    enum CodingKeys: String, CodingKey
    {
        case specItems = "special_items"
    }
    
    init() {
        
    }
}

class SpecialItem: Codable {
    var image: UIImage!
    var imgAdr: String = ""
    var category: String = ""
    
    enum CodingKeys: String, CodingKey
    {
        case imgAdr = "image"
    }
    
    init() {
        
    }
}
