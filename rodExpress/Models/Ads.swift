//
//  Ads.swift
//  rodExpress
//
//  Created by Dell Smith on 23/03/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import Foundation
import UIKit

class Ads: Codable {
    var image: UIImage?
    var imgAdr: String = ""
    var category: String = ""
    var url: URL!
    var urlString = ""
    
    init() {
        
    }
    
    enum CodingKeys: String, CodingKey
    {
        case imgAdr = "image"
        case urlString = "url"
    }
}

class AdsContainer: Codable {
    var Ads: [Ads] = []
    
    init() {
    }
}
