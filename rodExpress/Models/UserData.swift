//
//  userData.swift
//  rodExpress
//
//  Created by Dell Smith on 03/04/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import Foundation

class UserData {
    var userAddress: Address! = Address()
    var cardNumber: String! = ""
}

class Address {
    var country: String! = ""
    var city: String! = ""
    var street: String! = ""
    var house: String! = ""
    var appartmens: String! = ""
    var index: String! = ""
}
