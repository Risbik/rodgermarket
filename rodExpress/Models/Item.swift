//
//  Item.swift
//  rodExpress
//
//  Created by Dell Smith on 23/03/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import Foundation
import UIKit

class Item: Codable {
    var images: [UIImage] = []
    var imgAdr: [String] = []
    var cost: Int = 0
    var name: String = ""
    var desc: String = ""
    var producer: String = ""
    var category: String = ""
    var type: String = ""
    
    enum CodingKeys: String, CodingKey
    {
        case imgAdr = "images"
        case cost = "cost"
        case name = "name"
        case desc = "desc"
        case producer = "producer"
        case category = "category"
        case type = "type"
    }
    init() {
        
    }
}

class ItemContainer: Codable
{
    var Items: [Item] = []
    init() {
        
    }

}
