//
//  HintCell.swift
//  rodExpress
//
//  Created by Dell Smith on 26/03/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import UIKit

class HintCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
