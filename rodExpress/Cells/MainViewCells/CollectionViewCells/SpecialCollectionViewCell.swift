//
//  SpecialCollectionViewCell.swift
//  rodExpress
//
//  Created by Dell Smith on 23/03/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import UIKit

class SpecialCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
}
