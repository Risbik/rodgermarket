//
//  BasketViewCell.swift
//  rodExpress
//
//  Created by Dell Smith on 26/03/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import UIKit

class BasketViewCell: UITableViewCell {

    @IBOutlet weak var plus: UIButton!
    @IBOutlet weak var minus: UIButton!
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var cost: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    var basketItem: BasketItem!
    var _delegate: BasketCellDelegate!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func load() {
        number.text = String(basketItem.number)
        name.text = basketItem.item.name
        cost.text = String(basketItem.item.cost)
        imgView.image = basketItem.item.images[0]
        minus.addTarget(self, action: #selector(minusTapped), for: .touchUpInside)
        plus.addTarget(self, action: #selector(plusTapped), for: .touchUpInside)
        
        plus.layer.cornerRadius = 5
        plus.layer.borderWidth = 1
        plus.layer.borderColor = UIColor.white.cgColor
        
        minus.layer.cornerRadius = 5
        minus.layer.borderWidth = 1
        minus.layer.borderColor = UIColor.white.cgColor
        
        if (basketItem.number == 1) {
            disableMinus()
        }
    }
    
    func disableMinus() {
        minus.isEnabled = false
        minus.tintColor = UIColor.lightGray
        minus.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func enableMinus() {
        minus.isEnabled = true
        minus.layer.borderColor = UIColor.white.cgColor
        minus.tintColor = UIColor.orange
    }
    
    @objc func plusTapped() {
        var num = Int(number.text!)!
        if (num == 1) {
            enableMinus()
        }
        num += 1
        number.text = String(num)
        BasketViewModel.addToBasket(item: basketItem.item)
        _delegate.updateTotalCost()
    }
    
    @objc func minusTapped() {
        var num = Int(number.text!)!
        num -= 1
        if (num == 1) {
            disableMinus()
        }
        number.text = String(num)
        BasketViewModel.removeFromBasket(item: basketItem.item,  number: 1)
        _delegate.updateTotalCost()
    }

}
