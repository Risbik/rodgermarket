//
//  BasketButtonCell.swift
//  rodExpress
//
//  Created by Dell Smith on 27/03/2019.
//  Copyright © 2019 Dell Smith. All rights reserved.
//

import UIKit

class BasketButtonCell: UICollectionViewCell {
    @IBOutlet weak var label: UILabel!    
}
